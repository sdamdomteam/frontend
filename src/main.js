import Vue from 'vue'
import VueResource from 'vue-resource'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import GetMarkers from "./GetMarkers.vue"
import Insert from "./Insert.vue"
import GoogleMap from "./GoogleMap.vue"
//import {router} from './router'
import VueRouter from 'vue-router';
Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(BootstrapVue);
//import 'bootstrap';
//import 'bootstrap/dist/css/bootstrap.min.css';

var router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: GetMarkers },
        { path: '/insert', component: Insert },
        { path: '/map', component: GoogleMap },
        { path: '*', component: GoogleMap }
    ]
})

new Vue({
    el: '#app',
    router: router,
    render: h => h(App)
});
