import GetMarkers from "./GetMarkers.vue"
import GetOne from "./GetOne.vue"
import Insert from "./Insert.vue"
import GoogleMap from "./GoogleMap.vue"

export const routes = [
    {
        path: '/',
        component: GetMarkers
    },
    {
        path: '/insert',
        component: Insert
    },
    {
        path: '/map',
        component: GoogleMap
    },
    {
        path: '*',
        component: GoogleMap
    }
];
